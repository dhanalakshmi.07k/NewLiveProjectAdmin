singapp.controller('dashboard2Ctrl', function ($scope) {

 $scope.init_flot_chart=function(){

 		if( typeof ($.plot) === 'undefined'){ return; }

 		console.log('init_flot_chart');



 		var arr_data1 = [
 			[gd(2012, 1, 1), 17],
 			[gd(2012, 1, 2), 74],
 			[gd(2012, 1, 3), 6],
 			[gd(2012, 1, 4), 39],
 			[gd(2012, 1, 5), 20],
 			[gd(2012, 1, 6), 85],
 			[gd(2012, 1, 7), 7]
 		];

 		var arr_data2 = [
 		  [gd(2012, 1, 1), 82],
 		  [gd(2012, 1, 2), 23],
 		  [gd(2012, 1, 3), 66],
 		  [gd(2012, 1, 4), 9],
 		  [gd(2012, 1, 5), 119],
 		  [gd(2012, 1, 6), 6],
 		  [gd(2012, 1, 7), 9]
 		];

 		var arr_data3 = [
 			[0, 1],
 			[1, 9],
 			[2, 6],
 			[3, 10],
 			[4, 5],
 			[5, 17],
 			[6, 6],
 			[7, 10],
 			[8, 7],
 			[9, 11],
 			[10, 35],
 			[11, 9],
 			[12, 12],
 			[13, 5],
 			[14, 3],
 			[15, 4],
 			[16, 9]
 		];

 		var chart_plot_02_data = [];

 		var chart_plot_03_data = [
 			[0, 1],
 			[1, 9],
 			[2, 6],
 			[3, 10],
 			[4, 5],
 			[5, 17],
 			[6, 6],
 			[7, 10],
 			[8, 7],
 			[9, 11],
 			[10, 35],
 			[11, 9],
 			[12, 12],
 			[13, 5],
 			[14, 3],
 			[15, 4],
 			[16, 9]
 		];


 		for (var i = 0; i < 30; i++) {
 		  chart_plot_02_data.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
 		}


 		var chart_plot_01_settings = {
           series: {
             lines: {
               show: false,
               fill: true
             },
             splines: {
               show: true,
               tension: 0.4,
               lineWidth: 1,
               fill: 0.4
             },
             points: {
               radius: 0,
               show: true
             },
             shadowSize: 2
           },
           grid: {
             verticalLines: true,
             hoverable: true,
             clickable: true,
             tickColor: "#d5d5d5",
             borderWidth: 1,
             color: '#fff'
           },
           colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
           xaxis: {
             tickColor: "rgba(51, 51, 51, 0.06)",
             mode: "time",
             tickSize: [1, "day"],
             //tickLength: 10,
             axisLabel: "Date",
             axisLabelUseCanvas: true,
             axisLabelFontSizePixels: 12,
             axisLabelFontFamily: 'Verdana, Arial',
             axisLabelPadding: 10
           },
           yaxis: {
             ticks: 8,
             tickColor: "rgba(51, 51, 51, 0.06)",
           },
           tooltip: false
         }

 		var chart_plot_02_settings = {
 			grid: {
 				show: true,
 				aboveData: true,
 				color: "#3f3f3f",
 				labelMargin: 10,
 				axisMargin: 0,
 				borderWidth: 0,
 				borderColor: null,
 				minBorderMargin: 5,
 				clickable: true,
 				hoverable: true,
 				autoHighlight: true,
 				mouseActiveRadius: 100
 			},
 			series: {
 				lines: {
 					show: true,
 					fill: true,
 					lineWidth: 2,
 					steps: false
 				},
 				points: {
 					show: true,
 					radius: 4.5,
 					symbol: "circle",
 					lineWidth: 3.0
 				}
 			},
 			legend: {
 				position: "ne",
 				margin: [0, -25],
 				noColumns: 0,
 				labelBoxBorderColor: null,
 				labelFormatter: function(label, series) {
 					return label + '&nbsp;&nbsp;';
 				},
 				width: 40,
 				height: 1
 			},
 			colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
 			shadowSize: 0,
 			tooltip: true,
 			tooltipOpts: {
 				content: "%s: %y.0",
 				xDateFormat: "%d/%m",
 			shifts: {
 				x: -30,
 				y: -50
 			},
 			defaultTheme: false
 			},
 			yaxis: {
 				min: 0
 			},
 			xaxis: {
 				mode: "time",
 				minTickSize: [1, "day"],
 				timeformat: "%d/%m/%y",
 				min: chart_plot_02_data[0][0],
 				max: chart_plot_02_data[20][0]
 			}
 		};

 		var chart_plot_03_settings = {
 			series: {
 				curvedLines: {
 					apply: true,
 					active: true,
 					monotonicFit: true
 				}
 			},
 			colors: ["#26B99A"],
 			grid: {
 				borderWidth: {
 					top: 0,
 					right: 0,
 					bottom: 1,
 					left: 1
 				},
 				borderColor: {
 					bottom: "#7F8790",
 					left: "#7F8790"
 				}
 			}
 		};


         if ($("#chart_plot_01").length){
 			console.log('Plot1');

 			$.plot( $("#chart_plot_01"), [ arr_data1, arr_data2 ],  chart_plot_01_settings );
 		}


 		if ($("#chart_plot_02").length){
 			console.log('Plot2');

 			$.plot( $("#chart_plot_02"),
 			[{
 				label: "Email Sent",
 				data: chart_plot_02_data,
 				lines: {
 					fillColor: "rgba(150, 202, 89, 0.12)"
 				},
 				points: {
 					fillColor: "#fff" }
 			}], chart_plot_02_settings);

 		}

 		if ($("#chart_plot_03").length){
 			console.log('Plot3');


 			$.plot($("#chart_plot_03"), [{
 				label: "Registrations",
 				data: chart_plot_03_data,
 				lines: {
 					fillColor: "rgba(150, 202, 89, 0.12)"
 				},
 				points: {
 					fillColor: "#fff"
 				}
 			}], chart_plot_03_settings);

 		};

 	}




//donut
$scope.init_chart_doughnut=function(){

		if( typeof (Chart) === 'undefined'){ return; }

		console.log('init_chart_doughnut');

		if ($('.canvasDoughnut').length){

		var chart_doughnut_settings = {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: [
						"Symbian",
						"Blackberry",
						"Other",
						"Android",
						"IOS"
					],
					datasets: [{
						data: [15, 20, 30, 10, 30],
						backgroundColor: [
							"#BDC3C7",
							"#9B59B6",
							"#E74C3C",
							"#26B99A",
							"#3498DB"
						],
						hoverBackgroundColor: [
							"#CFD4D8",
							"#B370CF",
							"#E95E4F",
							"#36CAAB",
							"#49A9EA"
						]
					}]
				},
				options: {
					legend: false,
					responsive: false
				}
			}

			$('.canvasDoughnut').each(function(){

				var chart_element = $(this);
				var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);

			});

		}

	}




//sparkline
$scope.init_sparklines=function() {

			if(typeof (jQuery.fn.sparkline) === 'undefined'){ return; }
			console.log('init_sparklines');


			$(".sparkline_one").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'bar',
				height: '125',
				barWidth: 13,
				colorMap: {
					'7': '#a1a1a1'
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});


			$(".sparkline_two").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'bar',
				height: '40',
				barWidth: 9,
				colorMap: {
					'7': '#a1a1a1'
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});


			$(".sparkline_three").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 7, 5, 4, 3, 5, 6], {
				type: 'line',
				width: '200',
				height: '40',
				lineColor: '#26B99A',
				fillColor: 'rgba(223, 223, 223, 0.57)',
				lineWidth: 2,
				spotColor: '#26B99A',
				minSpotColor: '#26B99A'
			});


			$(".sparkline11").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3], {
				type: 'bar',
				height: '40',
				barWidth: 8,
				colorMap: {
					'7': '#a1a1a1'
				},
				barSpacing: 2,
				barColor: '#26B99A'
			});


			$(".sparkline22").sparkline([2, 4, 3, 4, 7, 5, 4, 3, 5, 6, 2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 6], {
				type: 'line',
				height: '40',
				width: '200',
				lineColor: '#26B99A',
				fillColor: '#ffffff',
				lineWidth: 3,
				spotColor: '#34495E',
				minSpotColor: '#34495E'
			});


			$(".sparkline_bar").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
				type: 'bar',
				colorMap: {
					'7': '#a1a1a1'
				},
				barColor: '#26B99A'
			});


			$(".sparkline_area").sparkline([5, 6, 7, 9, 9, 5, 3, 2, 2, 4, 6, 7], {
				type: 'line',
				lineColor: '#26B99A',
				fillColor: '#26B99A',
				spotColor: '#4578a0',
				minSpotColor: '#728fb2',
				maxSpotColor: '#6d93c4',
				highlightSpotColor: '#ef5179',
				highlightLineColor: '#8ba8bf',
				spotRadius: 2.5,
				width: 85
			});


			$(".sparkline_line").sparkline([2, 4, 3, 4, 5, 4, 5, 4, 3, 4, 5, 6, 4, 5, 6, 3, 5], {
				type: 'line',
				lineColor: '#26B99A',
				fillColor: '#ffffff',
				width: 85,
				spotColor: '#34495E',
				minSpotColor: '#34495E'
			});


			$(".sparkline_pie").sparkline([1, 1, 2, 1], {
				type: 'pie',
				sliceColors: ['#26B99A', '#ccc', '#75BCDD', '#D66DE2']
			});


			$(".sparkline_discreet").sparkline([4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 2, 4, 3, 7, 8, 9, 7, 6, 4, 3], {
				type: 'discrete',
				barWidth: 3,
				lineColor: '#26B99A',
				width: '85',
			});


		};





function init()
{
 $scope.init_flot_chart();
 $scope.init_chart_doughnut();
 $scope.init_sparklines();
}

  init();



}
)