var singapp = angular.module('helloApp', ['ui.router', 'angularFromUI', 'angularEditFromUI', 'ng-webcam', 'sorting-table', 'angularUtils.directives.dirPagination','ui.bootstrap', 'ui.bootstrap.datetimepicker','angular.chosen']);
singapp.run(function ( projectclientService,projectService, employeeprojectService,employeeService ) {

    function getprojectclientDetailsJsonConfig() {
        projectclientService.getprojectclientJsonConfig().then(function (resultDetails) {
        console.log("wertyui"+resultDetails.data);
            projectclientService.setprojectclientFromConfig(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })

    }

    function getprojectDetailsJsonConfig() {
        projectService.getprojectJsonConfig().then(function (resultDetails) {
            projectService.setprojectFromConfig(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
    }


    function getemployeeprojectDetailsJsonConfig() {
        employeeprojectService.getemployeeprojectJsonConfig().then(function (resultDetails) {
            employeeprojectService.setemployeeprojectFromConfig(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
    }
    function getemployeeDetailsJsonConfig() {
        employeeService.getemployeeJsonConfig().then(function (resultDetails) {
            employeeService.setemployeeFromConfig(resultDetails.data)
        }, function error(errResponse) {
            console.log("cannot get settings config")
        })
    }

    function init() {
        getprojectclientDetailsJsonConfig();
        getprojectDetailsJsonConfig();
        getemployeeprojectDetailsJsonConfig();
        getemployeeDetailsJsonConfig();

    }

    init();

});

singapp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

               $stateProvider
                   .state('login', {
                   url:'/login',
                    templateUrl:'client/login.html',
                     controller:'loginController'
                          })


                 $stateProvider
                 .state('dashboard', {
                    url:"/dashboard",
                    templateUrl: 'client/dashboardtwo.html',
                     controller:'dashboardCtrl'
                     })

        .state('projectclient', {
            url: "/projectclient",
           templateUrl: 'client/projectclient.html'
         })
        .state('projectclient.client', {
            url: "/clients",
            templateUrl: 'client/projectclientclient.html',
            controller: 'projectclientCtrl'
        })
        .state('projectclient.project', {
            url: "/project",
            templateUrl: 'client/project.html',
            controller: 'projectCtrl'
        })
        .state('employeeproject', {
                    url: "/employeeproject",
                    templateUrl: 'employee/employeeproject.html',
                    controller: 'projectCtrl'
                })
         .state('project', {
                             url: "/project",
                             templateUrl: 'client/project.html',
                             controller: 'projectCtrl'
                         })

        .state('projectclient.employeeproject', {
            url: "/employeeproject",
            templateUrl: 'client/employeeproject.html',
            controller: 'employeeprojectCtrl'
        })

         .state('status', {
                    url: "/status",
                    templateUrl: 'client/status.html',
                    controller:'userdailytaskCtrl'
                })

         .state('status2', {
                             url: "/status2",
                             templateUrl: 'client/status2.html',
                             controller:'userdailytaskCtrl'
                         })
       .state('employee', {
            url: "/employee",
            templateUrl: 'employee/employees.html'

        })

        .state('employee.employeess', {
            url: "/employeess",
            templateUrl: 'employee/employeess.html',
            controller: 'employeeCtrl'
        })

});


//singapp.directive('exportTable', function(){
//          var link = function ($scope, elm, attr) {
//            $scope.$on('export-pdf', function (e, d) {
//            elm.css('background-color', 'yellow');
//
////                elm.tableExport({ type: 'pdf', escape: false });
//            });
//            $scope.$on('export-excel', function (e, d) {
////                elm.tableExport({ type: 'excel', escape: false });
//            alert("hello");
//            });
//            $scope.$on('export-doc', function (e, d) {
//                elm.tableExport({ type: 'doc', escape: false });
//            });
//            $scope.$on('export-csv', function (e, d) {
//                elm.tableExport({ type: 'csv', escape: false });
//            });
//        }
//        return {
//            restrict: 'C',
//            link: link
//        }
//});



angular.module('helloApp').directive('exportTable', function () {
    var link = function ($scope, element, attr) {
            $scope.$on('export-pdf',function(e,d){
            element.exportDetails({ type:'pdf', escape: false });
            })

            $scope.$on('export-excel', function(e,d){
                element.exportDetails({ type:'excel', escape: false });
            });
            $scope.$on('export-doc',function(e,d){
                element. exportDetails({ type:'doc', escape: false});
            });
            $scope.$on('export-csv', function(e,d){
                element. exportDetails({ type:'csv', escape:false });
            });
    }
    return {
        restrict:'CA',
        link:link
    };
});